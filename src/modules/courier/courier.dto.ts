import { ApiProperty } from '@nestjs/swagger';

export class GetAllCourierResponseDto {
  @ApiProperty({
    type: String,
    description: 'Name of courier',
    example: 'jt',
  })
  courier: string;

  @ApiProperty({
    type: Number,
    description: 'Rate of courier',
    example: 10,
  })
  rate: number;
}

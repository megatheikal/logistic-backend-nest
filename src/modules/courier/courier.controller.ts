import { Controller, Get, UseInterceptors } from '@nestjs/common';
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { CourierService } from './courier.service';
import { GetAllCourierResponseDto } from './courier.dto';
import { CacheInterceptor } from '@nestjs/cache-manager';

@Controller()
@ApiTags('courier')
export class CourierController {
  constructor(private readonly courierService: CourierService) {}
  @Get('couriers')
  @UseInterceptors(CacheInterceptor)
  @ApiOperation({
    operationId: 'getAllCourier',
    description: 'Get all courier',
  })
  @ApiOkResponse({ type: [GetAllCourierResponseDto] })
  async getAllCourier(): Promise<GetAllCourierResponseDto[]> {
    return this.courierService.findAllCourier();
  }
}

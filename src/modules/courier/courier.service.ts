import { Injectable, Logger } from '@nestjs/common';
import { Courier } from './courier.entity';
import { CourierRepository } from './courier.repository';
import { GetAllCourierResponseDto } from './courier.dto';

@Injectable()
export class CourierService {
  constructor(private readonly courierRepository: CourierRepository) {}

  async findCourierById(id: string): Promise<Courier> {
    return this.courierRepository.findById(id);
  }

  async findAllCourier(): Promise<GetAllCourierResponseDto[]> {
    try {
      const output = await this.courierRepository.find({});
      return output.map((item) => {
        return { courier: item.name, rate: item.rate };
      });
    } catch (err) {
      Logger.error(`Something wrong to get all couriers ${err}`);
    }
  }
}

import { NestFactory } from '@nestjs/core';
import { AppModule } from './modules/app/app.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { INestApplication, Logger } from '@nestjs/common';
import helmet from 'helmet';
import { IConfig } from 'config';
import { CONFIG } from './modules/config/config.provider';

async function bootstrap() {
  const logger = new Logger('Main', { timestamp: true });
  const app = await NestFactory.create(AppModule);
  const config = app.get<IConfig>(CONFIG);

  app.useLogger(logger);

  initializeApp(app);
  initiliazeSwagger(app);
  app.use(helmet());

  const port = +config.get<number>('server.port');

  await app.listen(port);

  logger.log(
    `Application available on ${config.get<string>('server.swaggerSchema')}://${config.get<string>('server.host')}:${port}${config.get<string>('service.baseDocs')}`,
  );
}
bootstrap();

const initializeApp = (app: INestApplication) => {
  const config = app.get<IConfig>(CONFIG);

  app.setGlobalPrefix(config.get('service.baseUrl'));

  const corsOptions = {
    origin: '*',
    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE,OPTIONS',
    preflightContinue: false,
    optionsSuccessStatus: 204,
    credentials: true,
  };
  app.enableCors(corsOptions);
};

function initiliazeSwagger(app: INestApplication) {
  const config = app.get<IConfig>(CONFIG);
  const options = new DocumentBuilder()
    .setTitle(`${config.get<string>('service.name')} API spec`)
    .setDescription(`${config.get<string>('service.description')}`)
    .setVersion(`${config.get<string>('service.setVersion')}`)
    .build();

  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup(
    `${config.get<string>('service.baseDocs')}`,
    app,
    document,
  );
}

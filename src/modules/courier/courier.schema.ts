import { SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import * as paginate from 'mongoose-paginate-v2';
import { Courier } from './courier.entity';

export const CourierSchema = SchemaFactory.createForClass(Courier);
CourierSchema.plugin(paginate);

export interface ICourierDoc extends Document, Courier {
  _id: string;
}

import { Controller, Get, HttpCode, HttpStatus } from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';

@Controller()
@ApiTags('ping')
export class AppController {
  @Get('ping')
  @HttpCode(HttpStatus.OK)
  @ApiOperation({
    operationId: 'ping',
    description: 'Ping the controller',
  })
  getPing(): string {
    return 'pong!';
  }
}

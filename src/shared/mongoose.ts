import { NotFoundException } from '@nestjs/common';
import { Prop, SchemaOptions } from '@nestjs/mongoose';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { EventEmitter } from 'events';
import {
  Aggregate,
  Document,
  FilterQuery,
  PaginateModel,
  PaginateOptions,
  PaginateResult,
  PipelineStage,
  ProjectionType,
  QueryOptions,
  UpdateQuery,
} from 'mongoose';
import {} from 'mongoose-paginate-v2';

export class BaseEntity {
  constructor(input?: Partial<BaseEntity>) {
    if (input) {
      Object.assign(this, input);
    }
  }

  @ApiPropertyOptional({ type: 'string', format: 'date-time' })
  @Prop()
  createdAt?: Date;

  @ApiPropertyOptional({ type: 'string', format: 'date-time' })
  @Prop()
  updatedAt?: Date;

  @ApiProperty({
    type: 'string',
    description: 'System generated unique id',
  })
  _id?: string | any;
}

export const sanitizeData = (current, index?, prev?) => {
  if (current !== null && typeof current === 'object') {
    if (current.constructor.name === 'Decimal128') {
      prev[index] = Number(current.toString());
    } else if (current.constructor.name === 'ObjectID') {
      prev[index] = String(current);
    } else {
      Object.entries(current).forEach(([key, value]) =>
        sanitizeData(value, key, prev ? prev[index] : current),
      );
    }
  }
};

export const DEFAULT_SCHEMA_OPTIONS = (sanitize = false): SchemaOptions => {
  return {
    timestamps: true,
    toObject: {
      transform(_, ret) {
        delete Object.assign(ret, { [`id`]: ret[`id`] || ret[`_id`] })[`_id`];
        if (sanitize) {
          sanitizeData(ret);
        }
      },
      getters: true,
      virtuals: true,
      versionKey: false,
    },
    toJSON: {
      transform(_, ret) {
        delete Object.assign(ret, { [`id`]: ret[`id`] || ret[`_id`] })[`_id`];
        if (sanitize) {
          sanitizeData(ret);
        }
      },
      getters: true,
      virtuals: true,
      versionKey: false,
    },
  };
};

export interface IRepository<T extends Document> {
  getModel(): PaginateModel<T>;
  create(entity: Record<string, unknown>): Promise<T>;
  createMany(entities: Record<string, unknown>[]): Promise<T[]>;
  findById(id: string): Promise<T>;
  findOne(
    params: FilterQuery<T>,
    projection?: ProjectionType<T>,
    options?: QueryOptions<T>,
  ): Promise<T>;
  findOneOrFail(
    params: FilterQuery<T>,
    projection?: ProjectionType<T>,
    options?: QueryOptions<T>,
  ): Promise<T>;
  paginate(
    query: FilterQuery<T>,
    options: PaginateOptions,
  ): Promise<PaginateResult<T>>;
  findAll(
    filter: FilterQuery<T>,
    projection: ProjectionType<T>,
    options: PaginateOptions,
    limit: number,
    sort: { [key: string]: any },
  ): Promise<Array<T>>;
  removeOne(filter: FilterQuery<T>);
  removeById(id: string);
  findOneAndUpdate(filter: FilterQuery<T>, update: UpdateQuery<T>): Promise<T>;
  updateOne(filter: FilterQuery<T>, update: UpdateQuery<T>): Promise<T>;
  updateMany(filter: FilterQuery<T>, update: UpdateQuery<T>): Promise<void>;
  aggregate(pipeline: PipelineStage[]): Aggregate<Record<string, unknown>[]>;
}

export class BaseRepository<T extends Document>
  extends EventEmitter
  implements IRepository<T>
{
  protected primaryKey = '_id';

  constructor(protected readonly model: PaginateModel<T>) {
    super();
    this.model = model;
  }

  aggregate(pipeline: PipelineStage[]): Aggregate<Record<string, unknown>[]> {
    return this.model.aggregate(pipeline);
  }

  async create(entity: Record<string, unknown>): Promise<T> {
    return new this.model(entity).save();
  }

  async createMany(entities: Record<string, unknown>[]): Promise<T[]> {
    return this.model.create(entities);
  }

  async findById(id: string | number): Promise<T> {
    return this.model.findById(id).lean();
  }

  async findOne(
    params: FilterQuery<T>,
    projection?: ProjectionType<T>,
    options?: QueryOptions<T>,
  ): Promise<T> {
    return this.model.findOne(params, projection, {
      lean: true,
      ...options,
    });
  }

  async find(
    params: FilterQuery<T>,
    projection?: ProjectionType<T>,
    options?: QueryOptions<T>,
  ): Promise<T[]> {
    return this.model
      .find(params, projection, {
        lean: true,
        ...options,
      })
      .lean();
  }

  async findOneOrFail(params: FilterQuery<T>): Promise<T> {
    const model: T = await this.findOne(params);

    if (model === null) {
      throw new NotFoundException(
        `Model [${
          this.getModel().collection.name
        }] not found for query ${JSON.stringify(params)}`,
      );
    }

    return model;
  }

  async paginate(
    query: FilterQuery<T>,
    options: PaginateOptions,
  ): Promise<PaginateResult<T>> {
    return this.model.paginate(query, {
      ...options,
      lean: true,
    });
  }

  async findAll(
    filter: FilterQuery<T>,
    projection: ProjectionType<T>,
    options: PaginateOptions,
    limit = 0,
    sort: { [key: string]: any } = {},
  ): Promise<Array<T>> {
    const query = this.model
      .find(filter as any, projection, options)
      .limit(limit)
      .sort(sort);
    return query.exec();
  }

  getModel(): PaginateModel<T> {
    return this.model;
  }

  async removeById(id: string) {
    return this.model.findByIdAndDelete(id);
  }

  async removeOne(filter: FilterQuery<T>) {
    return this.model.findOneAndDelete(filter);
  }

  async findOneAndUpdate(
    filter: FilterQuery<T>,
    update: UpdateQuery<T>,
  ): Promise<T> {
    return this.model
      .findOneAndUpdate(filter, update, {
        upsert: true,
        new: true,
      })
      .exec();
  }

  async updateOne(filter: FilterQuery<T>, update: UpdateQuery<T>): Promise<T> {
    return this.findOneAndUpdate(filter, update);
  }

  async updateMany(
    filter: FilterQuery<T>,
    update: UpdateQuery<T>,
  ): Promise<void> {
    await this.model.updateMany(filter, update);
  }

  async deleteMany(filter: FilterQuery<T>, options = null): Promise<void> {
    await this.model.deleteMany(filter, options);
  }
}

import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { PaginateModel } from 'mongoose';
import { BaseRepository } from '../../shared/mongoose';
import { Courier } from './courier.entity';
import { ICourierDoc } from './courier.schema';

@Injectable()
export class CourierRepository extends BaseRepository<ICourierDoc> {
  constructor(
    @InjectModel(Courier.name)
    model: PaginateModel<ICourierDoc>,
  ) {
    super(model);
  }
}

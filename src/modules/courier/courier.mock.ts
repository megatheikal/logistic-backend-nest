import { GetAllCourierResponseDto } from './courier.dto';
import { Courier } from './courier.entity';

export const generateMockCourier = (
  override?: Partial<Courier>,
): Record<string, unknown> => {
  return {
    name: 'poslaju',
    rate: 10,
    ...override,
  };
};

export const generateMockGetAllCourierResponse =
  (): GetAllCourierResponseDto[] => {
    return [
      {
        courier: 'poslaju',
        rate: 10,
      },
    ];
  };

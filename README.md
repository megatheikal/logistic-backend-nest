## Logistic Backend Nest

Logistic backend using [Nest](https://github.com/nestjs/nest) framework TypeScript.

## Installation

```bash
$ docker run -d --name redis -p 6379:6379 redis
$ docker run -d --name mongodb -p 27017-27019:27017-27019 mongo
$ yarn install
```

## Running the app

```bash
# development
$ yarn run start

# watch mode
$ yarn run start:dev

# production mode
$ yarn run start:prod
```

## Test

```bash
# unit tests
$ yarn run test

# e2e tests
$ yarn run test:e2e

# test coverage
$ yarn run test:cov
```

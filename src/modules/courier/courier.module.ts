import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Courier } from './courier.entity';
import { CourierSchema } from './courier.schema';
import { CourierRepository } from './courier.repository';
import { CourierService } from './courier.service';
import { CourierController } from './courier.controller';
import { APP_INTERCEPTOR } from '@nestjs/core';
import { CacheInterceptor, CacheModule } from '@nestjs/cache-manager';
import { RedisClientOptions } from 'redis';
import { ConfigModule } from '@nestjs/config';
import { IConfig } from 'config';
import { redisStore } from 'cache-manager-redis-yet';
import { CONFIG } from '../config/config.provider';

@Module({
  imports: [
    CacheModule.registerAsync<RedisClientOptions>({
      imports: [ConfigModule],
      useFactory: async (config: IConfig) => ({
        store: await redisStore({
          socket: {
            host: config.get<string>('redis.host'),
            port: config.get<number>('redis.port'),
          },
          ttl: config.get<number>('redis.ttl'),
        }),
      }),
      inject: [CONFIG],
    }),
    MongooseModule.forFeature([{ name: Courier.name, schema: CourierSchema }]),
  ],
  controllers: [CourierController],
  providers: [
    CourierRepository,
    CourierService,
    { provide: APP_INTERCEPTOR, useClass: CacheInterceptor },
  ],
  exports: [],
})
export class CourierModule {}

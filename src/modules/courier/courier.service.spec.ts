import { Test, TestingModule } from '@nestjs/testing';
import { CourierService } from './courier.service';
import { CourierRepository } from './courier.repository';

import { Courier } from './courier.entity';
import { CourierSchema } from './courier.schema';
import { MongooseModule } from '@nestjs/mongoose';
import { DatabaseModule } from '../database/database.module';
import { generateMockCourier } from './courier.mock';

describe('CourierService', () => {
  let courierService: CourierService;
  let courierRepository: CourierRepository;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        DatabaseModule,
        MongooseModule.forFeature([
          { name: Courier.name, schema: CourierSchema },
        ]),
      ],
      providers: [CourierService, CourierRepository],
    }).compile();

    courierService = module.get<CourierService>(CourierService);
    courierRepository = module.get<CourierRepository>(CourierRepository);
  });

  it('should be defined', () => {
    expect(courierService).toBeDefined();
  });

  it('should return all list', async () => {
    //pre
    const input = generateMockCourier();
    const input2 = generateMockCourier({ name: 'jt', rate: 20 });
    await courierRepository.createMany([input, input2]);

    //action
    const response = await courierService.findAllCourier();
    const output = await courierRepository.findOne({ name: 'poslaju' });
    const output2 = await courierRepository.findOne({ name: 'jt' });

    //result
    expect(response).toBeDefined();
    expect(response[0].courier).toBe(output.name);
    expect(response[0].rate).toBe(output.rate);
    expect(response[1].courier).toBe(output2.name);
    expect(response[1].rate).toBe(output2.rate);
  });

  afterAll(async () => {
    await courierRepository.deleteMany({});
  });
});

import { Prop, Schema } from '@nestjs/mongoose';
import { BaseEntity, DEFAULT_SCHEMA_OPTIONS } from '../../shared/mongoose';

@Schema({ ...DEFAULT_SCHEMA_OPTIONS(), collection: 'courier' })
export class Courier extends BaseEntity {
  @Prop({ type: String, required: true })
  name: string;

  @Prop({ type: Number, required: true })
  rate: number;
}

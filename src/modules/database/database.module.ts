import { Global, Module } from '@nestjs/common';
import { MongooseModule, MongooseModuleOptions } from '@nestjs/mongoose';
import { IConfig } from 'config';
import { ConfigModule } from '../config/config.module';
import { CONFIG } from '../config/config.provider';

@Global()
@Module({
  imports: [
    MongooseModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: (config: IConfig): MongooseModuleOptions => {
        return {
          uri: config.get('mongodb.logistic.uri'),
        };
      },
      inject: [CONFIG],
    }),
  ],
})
export class DatabaseModule {}

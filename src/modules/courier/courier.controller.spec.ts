import { Test, TestingModule } from '@nestjs/testing';
import { DatabaseModule } from '../database/database.module';
import { MongooseModule } from '@nestjs/mongoose';
import { RedisClientOptions } from 'redis';
import { APP_INTERCEPTOR } from '@nestjs/core';
import { CacheInterceptor, CacheModule } from '@nestjs/cache-manager';
import { ConfigModule } from '@nestjs/config';
import { IConfig } from 'config';
import { redisStore } from 'cache-manager-redis-yet';
import { CourierController } from './courier.controller';
import { Courier } from './courier.entity';
import { CourierSchema } from './courier.schema';
import { CourierRepository } from './courier.repository';
import { CourierService } from './courier.service';

import { CONFIG } from '../config/config.provider';
import { generateMockGetAllCourierResponse } from './courier.mock';

describe('CourierController', () => {
  let courierController: CourierController;

  const mockedCourierService = {
    findAllCourier: jest.fn(),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        CacheModule.registerAsync<RedisClientOptions>({
          imports: [ConfigModule],
          useFactory: async (config: IConfig) => ({
            store: await redisStore({
              socket: {
                host: config.get<string>('redis.host'),
                port: config.get<number>('redis.port'),
              },
              ttl: config.get<number>('redis.ttl'),
            }),
          }),
          inject: [CONFIG],
        }),
        DatabaseModule,
        MongooseModule.forFeature([
          { name: Courier.name, schema: CourierSchema },
        ]),
      ],
      providers: [
        CourierService,
        CourierRepository,
        { provide: APP_INTERCEPTOR, useClass: CacheInterceptor },
      ],
      controllers: [CourierController],
    })
      .overrideProvider(CourierService)
      .useValue(mockedCourierService)
      .compile();

    courierController = module.get<CourierController>(CourierController);
  });

  it('should be display as expected', async () => {
    // pre
    const expectedOutput = generateMockGetAllCourierResponse();
    mockedCourierService.findAllCourier.mockResolvedValue(expectedOutput);

    //action
    const output = await courierController.getAllCourier();

    //assert
    expect(output).toBeDefined();
    expect(output).toBe(expectedOutput);
  });

  it('should be defined', () => {
    expect(courierController).toBeDefined();
  });
});
